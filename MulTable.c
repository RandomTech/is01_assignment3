#include <stdio.h>
int main(){
	int i, j, n; 
	printf("How many multiplication tables would you like to see?:\n");
	scanf("%d",&n);
	
	for(i=1; i<=10; i++){
	
		for(j=1; j<=n; j++){
			printf("  %dx%d = %d ", j,i,j*i);
		}
		printf("\n");
	}
	
	return 0;
}